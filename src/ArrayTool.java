public class ArrayTool {
    public static int closestToZero(int[] array){
        int[] defaultArray = {0};
        if (array.length == 0){
            array=defaultArray;
        }
        int result = array[0];
        for (int element : array) {
            if (isCloserToZero(element, result)) {
                result = element;
            }
        }
        return result;
    }

    private static boolean isCloserToZero(int challengerNumber, int championNumber) {
        boolean isChallengerCloserToZero = false;
        int squaredChampion = championNumber * championNumber;
        int squaredChallenger = challengerNumber * challengerNumber;
        if (squaredChallenger < squaredChampion) {
            isChallengerCloserToZero = true;
        }
        else if (squaredChallenger == squaredChampion) {
            if (challengerNumber > 0 ){
                isChallengerCloserToZero = true;
            }
        }

        return isChallengerCloserToZero;
    }

}
