import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBasicArrayTool {
    @Test
    public void testBasicClosestToZero(){
        int[] array = {0};
        assertEquals(0, ArrayTool.closestToZero(array));
    }

    @Test
    public void testBasicClosestToZero2(){
        int[] array = {1};
        assertEquals(1, ArrayTool.closestToZero(array));
    }

    @Test
    public void testLimitConditionClosestToZero(){
        int[] array = {};
        assertEquals(0, ArrayTool.closestToZero(array));
    }

    @Test
    public void mailExamplesToTest(){
        int[] array1 = {8, 5, 10};
        assertEquals(5, ArrayTool.closestToZero(array1));

        int[] array2 = {5, 4, -9, 6, -10, -1, 8};
        assertEquals(-1, ArrayTool.closestToZero(array2));

        int[] array3 = {8, 2, 3, -2};
        assertEquals(2, ArrayTool.closestToZero(array3));

        int[] array4 = {2, 0};
        assertEquals(0, ArrayTool.closestToZero(array4));

    }

    //when input is [8, 5, 10] it must returns 5
    //
    //when input is [5, 4, -9, 6, -10, -1, 8] it must return -1
    //
    //when input is [8, 2, 3, -2] it must return 2
    //
    //when input is [2, 0], it must returns 0, as 0 is ... the closest number to 0


}
